# PROYECTO PAREA

## ENTORNO DE TRABAJO

Para crear el entorno de trabajo y que sea fácilmente exportable a otros equipos se han adaptado los pasos vistos en esta guía: https://www.digitalocean.com/community/tutorials/containerizing-a-node-js-application-for-development-with-docker-compose-es

Para que funcione, deberías crear el archivo `.env` con las variables de entorno básicas para conectarse a la base de datos Mongo:

```
MONGO_USERNAME=user
MONGO_PASSWORD=password
MONGO_PORT=27017
MONGO_DB=database
TOKEN_SECRET=supersecret
```

### Problemas conocidos

El archivo `whait-for.sh` debería tener permisos de ejecución (`sudo chmod +x whait-for.sh`) y tener una codificación de salto de línea de LF y no de CRLF. Esto pasa por no configurar bien el entorno Git, cosa que voy a intentar solucionar con la configuración del archivo `.gitattributes`.
