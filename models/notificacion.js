const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Notificacion = new Schema({
    id_creador: { type: Schema.Types.ObjectId, ref: 'Usuario', required: true },
    receptores: [
        { type: Schema.Types.ObjectId, ref: 'Usuario', required: true }
    ],
    texto: { type: String, default: '', required: true },
    titulo: { type: String, default: 'Sin asunto' },
    fecha_creado: { type: Date, default: Date.now },
    visto: { type: Boolean, default: false },
    tipo: { type: String, enum: ['mensaje', 'alerta', 'invitacion', 'suspension', 'recordatorio'], default: 'mensaje' },
    enviado: { type: String, default: 'ahora' },
    favorito: { type: Boolean, default: false },
    propio: { type: Boolean, default: false },
    borrado: { type: Boolean, default: false },
})

// funciones estáticas
Notificacion.statics.getById = function (id) {
    return this.findOne({ _id: id }).lean()
}

// Notificacion.statics.getAll = function (name, readed = false) {
//     return this.find({ _id: { $in: usuario.amigos } }).sort({ fecha_creado: 'asc' })
// }

module.exports = mongoose.model('Notificacion', Notificacion)