const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcryptjs = require('bcryptjs')

const Usuario = new Schema({
    nombre: { type: String, required: true, unique: [true, 'El nombre de usuario ya existe'] },
    f_nacimiento: { type: Date },
    email: { type: String, required: true, unique: [true, 'El email ya existe'] },
    password: { type: String, required: [true, 'El password es requerido'], select: false },
    avatar: { type: String, default: 'https://i.pravatar.cc/300?u=' + this.nombre },
    token: { type: String, unique: [true, 'El token debe ser único'] },
    rol: { type: String, default: undefined },
    calificacion: { type: Number, default: 0 },
    meta: {
        fecha_alta: { type: Date, required: true, default: Date.now },
        validado: { type: Date },
        ultima_conexion: { type: Date },
    },
    suspendido: {
        required: false,
        suspendido_por: { type: Schema.Types.ObjectId, ref: 'Usuario' },
        tipo: { type: String, enum: ['temporal', 'permanente'] },
        texto: { type: String },
        fecha_termino: { type: Date },
    },
    advertencias: [{ type: Schema.Types.ObjectId, ref: 'Advertencia' }],
    imparte: [{ type: Schema.Types.ObjectId, ref: 'Conocimiento' }],
    intereses: [{ type: Schema.Types.ObjectId, ref: 'Conocimiento' }],
    amigos: [{ type: Schema.Types.ObjectId, ref: 'Usuario', uniqueItems: true }],
    bloqueados: [{ type: Schema.Types.ObjectId, ref: 'Usuario', uniqueItems: true }],
    invitaciones: [{ type: Schema.Types.ObjectId, ref: 'Notificacion' }],
    friend: { type: Boolean, default: false },
})

Usuario.pre('save', function (next) {
    // console.log(this.nombre)
    if (this.isNew) {
        this.token = generateToken()
        this.password = encryptPassword(this.password)
    }

    // if (typeof this._update.password !== undefined)
    //     console.log(this._update.password)
    this.meta.ultima_conexion = Date.now()
    next()
})

Usuario.pre('findOneAndUpdate', function (next) {
    if (this._update?.password) // comprobamos si el password fue modificado para encriptarlo
        this._update.password = encryptPassword(this._update.password)

    next()
})

// eliminamos el password en las respuestas JSON
// Usuario.methods.toJSON = function () {
//     let usuario = this.toObject()
//     delete usuario.password
//     return usuario
// }

// generamos un token aleatorio con caracteres hexadecimales
function generateToken() {
    const token = Buffer.from(
        Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15), 'utf8'
    ).toString('hex')
    return token
}

function encryptPassword(password) {
    // return bcryptjs.hashSync(password, 10)
    let salt = bcryptjs.genSaltSync(10) // 10 es la cantidad de iteraciones
    password = bcryptjs.hashSync(password, salt) // encriptamos el password
    return password
}

Usuario.statics.getByName = function (name, pass = false) {
    const filter = (pass) ? '+password' : '-password' // filtro para no traer el password
    return this.findOne({ nombre: name }).select(filter)
}

Usuario.statics.getIDByName = function (name) {
    return this.findOne({ nombre: name }).select('_id')
}

module.exports = mongoose.model('Usuario', Usuario)