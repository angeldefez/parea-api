const mongoose = require('mongoose')
const Schema = mongoose.Schema

const jitsi_url = 'https://meet.jit.si/'

const Quedada = new Schema({
    id_creador: { type: Schema.Types.ObjectId, ref: 'Usuario', required: true },
    descripcion: { type: String, default: '' },
    fecha_creado: { type: Date, default: Date.now },
    fecha_quedada: { type: Date, default: Date.now, required: true },
    duracion: { type: Number, default: 2 },
    enlace: { type: String, unique: true },
    // enlace_invitado: { type: String, unique: true },
    estado: { type: String, enum: ['pendiente', 'activa', 'cancelada', 'terminada'], default: 'pendiente' },
    participantes: [
        { type: Schema.Types.ObjectId, ref: 'Usuario' }],
    puntuaciones: [{
        id_usuario: { type: Schema.Types.ObjectId, ref: 'Usuario', required: true },
        nota: { type: Number, min: 0, max: 5, default: 3 }
    }],
    comentarios: [{
        id_usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
        comentario: { type: String, default: '' }
    }],
    denuncias: [{
        id_usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
        fecha: { type: Date, default: Date.now },
        motivo: { type: String, required: true }
    }],
    propio: { type: Boolean, default: false }
})

Quedada.pre('save', function (next) {
    if (this.isNew) {
        this.enlace = randomLink()
        this.participantes.push(this.id_creador)
    }
    next()
})

// funciones estáticas
Quedada.statics.getById = function (id) {
    return this.findOne({ _id: id }).lean()
}

Quedada.statics.getAll = function () {
    return this.find().sort({ nombre: 'asc' })
}

function randomLink(n = 16) {
    return jitsi_url + require('crypto').randomBytes(n).toString('hex')
}

module.exports = mongoose.model('Quedada', Quedada)