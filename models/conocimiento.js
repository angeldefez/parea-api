const mongoose = require('mongoose')
const Schema = mongoose.Schema

// const Hijo = new Schema({
//     nombre: { type: String, unique: true },
//     icono: { type: String, default: 'base' },
//     descripcion: { type: String, default: '' },
//     pendiente_aprobar: { type: Boolean, default: false },
//     solicitado_por: [
//         { type: Schema.Types.ObjectId, ref: 'Usuario', required: true },
//         { type: Date, default: Date.now }
//     ],
//     hijos: [{
//         type: Schema.Types.Mixed,
//         _id: new mongoose.Types.ObjectId().toHexString()
//     }]
// })

const Conocimiento = new Schema({
    nombre: { type: String, unique: true },
    icono: { type: String, default: 'base' },
    descripcion: { type: String, default: '' },
    pendiente_aprobar: { type: Boolean, default: false },
    solicitado_por: [
        { type: Schema.Types.ObjectId, ref: 'Usuario', required: true },
        { type: Date, default: Date.now }
    ],
    hijos: [{ type: Schema.Types.Mixed, _id: new mongoose.Types.ObjectId().toHexString() }]
})

Conocimiento.pre('validate', function (next, data) {
    // check if pendiente_aprobar is empty
    if (this.pendiente_aprobar.length == -1)
        this.pendiente_aprobar = undefined

    next()
})

// funciones estáticas
Conocimiento.statics.getById = function (id) {
    return this.findOne({ _id: id }).lean() // con lean() es más rápido pero se convierte el objeto a JSON ** cuidado **
}

Conocimiento.statics.getAll = function () {
    return this.find().sort({ nombre: 'asc' })
}

Conocimiento.statics.getByName = async function (nombre) {
    return await this.findOne({ nombre }).lean() // con lean() es más rápido pero se convierte el objeto a JSON ** cuidado **
}

module.exports = mongoose.model('Conocimiento', Conocimiento)