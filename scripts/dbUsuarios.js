const express = require('express');
//const app = express();
const db = require('../db');

//const path = require('path');
const Usuario = require('../models/usuario');
// Usuario.collection.drop() // drop collection

//Bind connection to error event (to get notification of connection errors)
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var dbUsuarios = csvJSON('parea_usuario.csv')
// console.log(JSON.parse(dbUsuarios));

//var csv is the CSV file with headers
function csvJSON(file) {

    var fs = require('fs');
    var csv = fs.readFileSync(__dirname + '/' + file, 'utf8');
    csv = csv.replace(/\r/g, '');
    var lines = csv.split("\n");

    var result = [];
    var headers = lines[0].replace(/\s/g, '').split(";");

    for (var i = 1; i < lines.length; i++) {

        var obj = {};
        var currentline = lines[i].split(";");

        for (var j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }

        result.push(obj);

    }
    return result //JSON.stringify(result); //JSON
}
function poblar() {
    for (var i in (dbUsuarios)) {
        // for (var key in dbUsuarios[i]) {
        //     if (dbUsuarios[i][key] === null || dbUsuarios[i][key] === undefined || dbUsuarios[i][key] === "" || dbUsuarios[i][key].length === 0)
        //         delete dbUsuarios[i][key]
        // }

        // getPadreID(dbUsuarios[i])
        var newUsuario = new Usuario(dbUsuarios[i])
        // getPadreID(newUsuario)
        // console.log(newUsuario)

        // dbUsuarios[i].fecha_alta = new Date(dbUsuarios[i].fecha_alta)
        // dbUsuarios[i].ultima_conexion = new Date(dbUsuarios[i].ultima_conexion)
        newUsuario.meta = { fecha_alta: dbUsuarios[i].fecha_alta, ultima_conexion: dbUsuarios[i].ultima_conexion }

        newUsuario.save(function (err, usuario) {
            if (err) {
                // console.log(err)
                // nombre = (err.message.match(/"([^"]*)"/)[1]) // regex capture only word between quotes
                switch (err.code) {
                    case 11000:
                        console.log(`[-] ${this.nombre} ya existe`)
                        break;
                    default:
                        console.log(`[-] ${this.nombre} error al guardar`)
                        console.log(err);
                }
            }
            else {
                console.log(`[+] ${usuario.nombre} saved`);
                // console.log(usuario)
            }
        })
    }
}

// find Comocimiento where parent is null
var usuarios = new Promise(function (resolve, reject) {
    Usuario.find({ rol: "", calificacion: { $gt: 3 } }, function (err, usuario) {
        if (err) {
            reject(err);
        }
        else {
            resolve(usuario);
        }
    })
}).then(function (usuario) {
    var users = []
    for (var i in usuario) {
        console.log(`[+] usuario: ${usuario[i].nombre} : ${usuario[i].token}`);
        users.push(usuario[i])
    }
    return users
})

poblar()

