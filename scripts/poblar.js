const express = require('express')
const db = require('../db')
const Usuario = require('../models/usuario')
const Conocimiento = require('../models/conocimiento')

const adm = {
    nombre: 'admin',
    email: 'admin@mail.com',
    password: 'supersecret!'
}

// const admin = new Usuario(adm).save()

async function poblar() {
    usuarios = []
    await Usuario.find({}).then(async function (usuarios) {
        // console.log(usuarios.length)
        for (usuario in usuarios) {
            await randomaddfriend(usuarios[usuario])
            await randomaddskill(usuarios[usuario])
            await usuarios[usuario].save(function (err, usuario) {
                if (err)
                    console.log(err)
                else
                    console.log('[+] ' + usuario.nombre + ' saved')
            })
        }
    })

    console.log('Poblando usuarios')
}

poblar()



async function randomaddfriend(usuario) {
    let r = Math.random() * 10 | 1
    await Usuario.aggregate([{ $sample: { size: r } }], function (err, amigos) {
        for (amigo in amigos) {
            usuario.amigos.push(amigos[amigo]._id)
        }
    })
}

async function randomaddskill(usuario) {
    let r = Math.random() * 6 | 2
    await Conocimiento.aggregate([{ $sample: { size: r } }], function (err, conocimientos) {
        for (conocimiento in conocimientos) {
            let area = Math.floor(Math.random() * 2) == 0 ? 'imparte' : 'intereses'
            usuario[area].push(conocimientos[conocimiento]._id)
        }
    })
}
