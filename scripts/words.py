# -*- coding: utf-8 -*-

import csv
import json
import random

# open file in current folder
with open('words.csv', 'r', encoding='utf-8') as f:
    reader = csv.reader(f, delimiter=';')
    # create list for each header
    header = next(reader)
    # create dictionary with header as key
    data = {key: [] for key in header}
    # add each row to dictionary
    for row in reader:
        for key, val in zip(header, row):
            # add to list if not empty
            if val:
                data[key].append(val)
# print(data['num'])

dict = {}
list = []

for i in range(len(data['num'])):
    d = 'wordle' if random.randint(1, 10) > 1 else 'diceware'
    w = random.choice(data[d])
    # do while word is already in list
    while w in list:
        w = random.choice(data[d])

    list.append(w)

# order list alphabetically
list.sort()

for i in range(len(data['num'])):
    dict[data['num'][i]] = list[i]

# save to json file
with open('words.json', 'w', encoding='utf-8') as f:
    json.dump(dict, f, ensure_ascii=False, indent=4)

p = ''
for i in range(0, 6):
    w = ''
    n = ''
    for i in range(0, 5):
        n += str(random.randint(1, 6))
    print(n)
    w = dict[n]
    p += w.capitalize() if random.randint(1, 10) > 3 else '-'+w.lower()
print(p)
