const express = require('express');
//const app = express();
const db = require('../db');

//const path = require('path');
const Conocimiento = require('../models/conocimiento');

//Bind connection to error event (to get notification of connection errors)
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var dbConocimientos = csvJSON('dbConocimientos.csv')
// console.log(JSON.parse(dbConocimientos));

//var csv is the CSV file with headers
function csvJSON(file){

    var fs = require('fs');
    var csv = fs.readFileSync(__dirname+'/'+file, 'utf8');
    csv = csv.replace(/\r/g, '');
    var lines=csv.split("\n");
  
    var result = [];
    var headers=lines[0].replace(/\s/g, '').split(";");
  
    for(var i=1;i<lines.length;i++){
  
        var obj = {};
        var currentline=lines[i].split(";");
  
        for(var j=0;j<headers.length;j++){
            obj[headers[j]] = currentline[j];
        }
        
        obj.hijos = []
        console.log(`papa:${obj.padre}...`)
        result.push(obj);
  
    }
    return result //JSON.stringify(result); //JSON
}

// for (var i in (dbConocimientos))
// getPadreID(dbConocimientos[i])

// wait 5 seconds before running the function
// setTimeout(poblar, 5000);

function mezclar() {
    for (let i in dbConocimientos.reverse()) {
        let conocimiento = dbConocimientos[i]
        if (conocimiento.padre === undefined || conocimiento.padre === null || conocimiento.padre === '')
            break;
        let conPadre = dbConocimientos.find((padre) => {
            return padre.nombre === conocimiento.padre
        })

        if (conPadre) {
            // console.log(`encontrado hijo: ${conocimiento.nombre} padre: ${conPadre}`)
            conPadre.hijos.push(conocimiento)
            // dbConocimientos.splice(i, 1)
        }
        // console.log(`conPadre: ${conPadre.nombre} => ${conPadre.hijos.map(h => h.nombre)}`)
    }
}

function limpiar() {
    for (let i in dbConocimientos) {
        let con = dbConocimientos[i]
        if (con.padre === undefined || con.padre === null || con.padre === '') {
            dbConocimientos.splice(i,1)
            console.log(`[+] ${con.nombre} padre: ${con.padre}`)
        }
    }
}

// drop database 'parea' waiting
mezclar()
limpiar()
poblar()

// show all Conocimientos
let conocimientos =  
    Conocimiento.find(function (err, conocimientos) {
    if (err)
        console.log(err);
    return JSON.stringify(conocimientos); // devuelve todos los conocimientos en JSON
    })

// console.log(conocimientos)

function poblar() {
for (var i in (dbConocimientos)) {
    // for (var key in dbConocimientos[i]) {
    //     if (dbConocimientos[i][key] === null || dbConocimientos[i][key] === undefined || dbConocimientos[i][key] === "" || dbConocimientos[i][key].length === 0)
    //         delete dbConocimientos[i][key]
    // }
    
    // getPadreID(dbConocimientos[i])
    var newConocimiento = new Conocimiento(dbConocimientos[i])
    // getPadreID(newConocimiento)
    // console.log(newConocimiento)
    newConocimiento.save(function (err, conocimiento) {
        if(err) {
            console.log(err)
            nombre = (err.message.match(/"([^"]*)"/)[1]) // regex capture only word between quotes
            switch (err.code) {
                case 11000:
                    console.log(`[-] ${nombre} ya existe`)
                    break;
                default:
                    console.log(`[-] ${nombre} error al guardar`)
                    console.log(err);
            }
        }
        else
            console.log(`[+] ${conocimiento.nombre} saved`);
    })
}
}

function getPadreID2(con) {
    var id = new Promise((resolve, reject) => {
        Conocimiento.findOne().where('nombre').equals(con.padre).exec((err, conocimiento) => {
            if (!err) {
                id = conocimiento ? conocimiento._id : undefined
                console.log(`[+] ${con.nombre} padre: ${id}`)
                resolve(id)
            }
            else {
                console.log(err)
                reject(err)
            }
        })
    }).then(() => {
        con.padre = id
        return con
    })
}

function getPadreID(con) {
    Conocimiento.findOne({ 'nombre': con.padre }, (err, conocimiento) => {
        if (!err) {
            id = (con.padre && conocimiento) ? conocimiento._id : undefined
            console.log(`[+] ${con.nombre} padre: ${id}`)
        }
        else {
            console.log(err)
        }
    })
}

// find Comocimiento where parent is null

// lista = {}
// Conocimiento.find({padre : { $exists: false }}, function (err, conocimientos) {
//     if (err) {
//         console.log(err);
//     }
//     else {
//         lista = conocimientos
//         // console.log(conocimientos);
//     }
// })

// console.log((lista))

var arte =   {
    "nombre": "Arte",
    "icono": "arte",
    "descripcion": "Arte",
    "hijos": [
      {
        "nombre": "Música",
        "icono": "arte",
        "descripcion": "musicología"
      },
      {
        "nombre": "Pintura",
        "icono": "arte",
        "descripcion": "pinturología"
      }
    ]
  }

let newConocimiento = new Conocimiento(arte);
newConocimiento.save(function (err, conocimiento) {
    if(err) {
        console.log(err)
        nombre = (err.message.match(/"([^"]*)"/)[1]) // regex capture only word between quotes
        switch (err.code) {
            case 11000:
                console.log(`[-] ${nombre} ya existe`)
                break;
            default:
                console.log(`[-] ${nombre} error al guardar`)
                console.log(err);
        }
    }
    else
        console.log(`[+] ${conocimiento.nombre} saved`);
})