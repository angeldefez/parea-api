// const express = require('express')
const db = require('../db')
const fs = require('fs');

const colecciones =
{
    'conocimiento': 'conocimientos.json',
    'usuario': 'usuarios.json',
    'quedada': 'quedadas.json',
    'notificacion': 'notificaciones.json',
}

importar(colecciones)

function importar(colecciones) {
    for (const [modelo, archivo] of Object.entries(colecciones)) {
        let coleccion = fs.readFileSync(`./scripts/${archivo}`, 'utf8')
        let datos = JSON.parse(coleccion)

        const Model = require(`../models/${modelo}`)
        Model.collection.drop()

        // db.city.insertMany(cities)
        Model.insertMany(datos, (err, result) => {
            if (err) {
                console.log(err)
                return false
            }
            else {
                console.log(`[+] ${archivo} importado`)
                return true
            }
        })
    }
}