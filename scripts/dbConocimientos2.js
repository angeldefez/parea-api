const express = require('express');
//const app = express();
const db = require('../db');

//const path = require('path');
const Conocimiento = require('../models/conocimiento');

// read json file into an array
var fs = require('fs')
var obj = JSON.parse(fs.readFileSync(__dirname+'/dbConocimientos.json', 'utf8'))

for (con in obj)
    guardar(new Conocimiento(obj[con]))

function guardar(con) {
    con.save(function (err, conocimiento) {
        if(err) {
            console.log(err)
            nombre = (err.message.match(/"([^"]*)"/)[1]) // regex capture only word between quotes
            switch (err.code) {
                case 11000:
                    console.log(`[-] ${nombre} ya existe`)
                    break;
                default:
                    console.log(`[-] ${nombre} error al guardar`)
                    console.log(err);
            }
        }
        else
            console.log(`[+] ${conocimiento.nombre} saved`)
    })
}