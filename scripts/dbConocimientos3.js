const express = require('express');
//const app = express();
const db = require('../db');

//const path = require('path');
const Conocimiento = require('../models/conocimiento');
Conocimiento.collection.drop() // drop collection

//Bind connection to error event (to get notification of connection errors)
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var dbConocimientos = csvJSON('dbConocimientos.csv')
// console.log(JSON.parse(dbConocimientos));

//var csv is the CSV file with headers
async function csvJSON(file) {

    console.log(`[+] ${file}`)

    var fs = require('fs');
    var csv = fs.readFileSync(__dirname + '/' + file, 'utf8');
    csv = csv.replace(/\r/g, '');
    var lines = csv.split("\n");

    var result = [];
    var headers = lines[0].replace(/\s/g, '').split(";");

    for (var i = 1; i < lines.length; i++) {

        var obj = {};
        var currentline = lines[i].split(";");

        for (var j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }

        // obj.hijos = []
        // console.log(`papa:${obj.padre}...`)
        // result.push(obj);

        let newConocimiento = await new Conocimiento(obj)
        // newConocimiento.hijos = []
        if (obj.padre === '') {
            const o = await guardar(newConocimiento)
            // await wait(100)
            console.log(`[+] ${newConocimiento.nombre}`);
        } else {
            // console.log(`[+] ${newConocimiento.nombre} - padre: ${obj.padre} `)
            const papa = await hijo(newConocimiento, obj.padre)
            // await wait(100)
            if (papa) {
                // let hijos = papa.hijos
                // hijos.push(newConocimiento)
                const p = guardar(newConocimiento)
            }
            // await wait()
            // console.log(h)
        }

    }
    return result //JSON.stringify(result); //JSON
}

function wait(ms = 1000) {
    return new Promise(resolve => setTimeout(() => resolve(), ms));
}

function guardar(con) {
    let s = con.save((err) => {
        if (err)
            console.log('[-] ' + con.nombre + ' ya existe')
    })
}

async function hijo(hijo, padre) {
    let papa = await getPadre(padre)
    if (papa) {
        papa.hijos.push(hijo)
        await papa.save((err, conocimiento) => {
            if (!err) {
                // res.json(usuario)
                console.log(`[^] ${hijo.nombre} - padre: ${padre} `)
            }
        })
    }
    // await wait(500)
}

// wait()
// const a = getPadre('Hogar')
// console.log(a)

async function getPadre(nombre) {
    let c = await Conocimiento.findOne({ nombre })
    await wait(300)
    return c
}

// show all Conocimientos
// let conocimientos =
//     Conocimiento.find().lean().then(function (err, conocimientos) {
//         if (err)
//             console.log(err);
//         else
//             return conocimientos;
//     })

// console.log(conocimientos)