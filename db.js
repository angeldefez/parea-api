const mongoose = require('mongoose')

const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_DB
} = process.env

const options = {
    useNewUrlParser: true,
    //reconnectTries: Number.MAX_VALUE,
    //reconnectInterval: 500,
    connectTimeoutMS: 20000,
    //useCreateIndex: true,
    //createIndexes: true,
    autoIndex: true,
    useUnifiedTopology: true,
}

const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`

mongoose.connect(url, options).then(function () {
    console.log('[+] conectado a MongoDB...')
    // show mongoose version
    console.log(`[+] Mongoose version: ${mongoose.version} | Node version: ${process.version}`)
})
    .catch(function (err) {
        console.log('[x] No se ha podido conectar a la BBDD MongoDB:', err)
    })

module.exports = mongoose