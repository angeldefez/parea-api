FROM node:17.7-alpine3.14

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY ./app/wait-for.sh ./app/wait-for.sh

RUN sed 's/^M$//' ./app/wait-for.sh > ./app/wait-for.sh

RUN chmod 744 ./app/wait-for.sh

COPY package*.json ./

RUN chown node:node ./package*.json

USER node

RUN npm install

COPY --chown=node:node . .

EXPOSE 8080

CMD [ "node", "app.js" ]
