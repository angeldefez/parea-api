const express = require('express')
const api = express.Router()
const { authToken } = require('./middlewares/jwt')

// rutas
const user = require('./routes/user')
const skills = require('./routes/skills')
const meets = require('./routes/meets')
const notice = require('./routes/notice')
const mongo = require('./routes/mongo')

// con token de autenticación
api.use('/user', authToken, user)
api.use('/skills', authToken, skills)
api.use('/meets', authToken, meets)
api.use('/notice', authToken, notice)

// sin token de autenticación
api.use('/', user)
api.use('/login', user)
api.use('/mongo', mongo)

module.exports = api