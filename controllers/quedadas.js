const path = require('path')
const Quedada = require('../models/quedada')
const Usuario = require('../models/usuario')
const limpiar = require('../middlewares/limpiar')

exports.list = async (req, res) => {
        try {
                const user = await Usuario.getByName(limpiar(req.user.name))
                if (user) {
                        Quedada.find({
                                $or: [
                                        { id_creador: user._id },
                                        { participantes: user._id }
                                ]
                        })
                                .sort({ fecha_quedada: 'desc' })
                                .populate('participantes')
                                .populate('id_creador', 'nombre')
                                .then(function (meets) {
                                        for (let m in meets)
                                                meets[m].propio = meets[m].id_creador.equals(user._id)
                                        res.status(200).json(meets)
                                })
                } else {
                        res.status(500).send('Usuario no encontrado')
                }
        } catch (error) {
                console.log(error)
                res.status(500).send(error)
        }
}

exports.create = async (req, res) => {
        try {
                req.body.id_creador = await Usuario.getByName(limpiar(req.user.name))
                const data = await Quedada.create(limpiar(req.body))
                res.json(data)
        } catch (error) {
                res.status(500).send(error)
        }
}

exports.update = (req, res) => {
        try {
                Quedada.findOneAndUpdate(limpiar(req.params.id), limpiar(req.body)).then(function (err, quedada) {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        res.status(200).json(quedada)
                })
        } catch (error) {
                res.status(500).send(error)
        }
}

exports.delete = (req, res) => {
        try {
                Quedada.findByIdAndRemove(limpiar(req.params.id), (err, quedada) => {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        res.status(200).send('Quedada eliminada')
                })
        } catch (error) {
                res.status(500).send(error)
        }
}


exports.findById = async (req, res) => {
        try {
                const data = await Quedada.getById(limpiar(req.params.id))
                res.json(data)
        } catch (error) {
                res.status(500).send(error)
        }
}