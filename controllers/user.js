const path = require('path')
const Usuario = require('../models/usuario')
const bcryptjs = require('bcryptjs')
const { generateAccessToken } = require('../middlewares/jwt')
const limpiar = require('../middlewares/limpiar')

exports.get = async function (req, res) {
        try {
                let user = await Usuario.getByName(limpiar(req.user.name))
                if (user)
                        res.status(200).send(user)
                else
                        res.status(500).send('Usuario no encontrado')
                //         Usuario.findOne({ nombre: req.user.name }, function (err, usuario) {
                //                 if (err) {
                //                         return res.status(500).send(err)
                //                 }
                //                 res.status(200).json(usuario)
                //         })
        } catch (err) {
                res.status(500).send(err)
        }
}

// actualiza un usuario con los datos del formulario
exports.update = async function (req, res) {
        try {
                let user = await Usuario.getByName(limpiar(req.user.name))
                if (user) {
                        Usuario.findByIdAndUpdate(user._id, limpiar(req.body), (err, usuario) => {
                                if (err) {
                                        console.log(err)
                                        res.status(500).send(err)
                                }
                                // res.json(usuario)
                                res.status(200).send('Usuario actualizado')
                        })
                }
        } catch (err) {
                res.status(500).send(err)
        }
}

// login
exports.login = async function (req, res) {
        try {
                let user = await Usuario.getByName(limpiar(req.body.user), true).catch(err => {
                        console.log(err)
                })
                if (user) {
                        const passwordSucces = await bcryptjs.compare(limpiar(req.body.password), user.password)
                        if (!passwordSucces) {
                                res.status(401).send('Password o usuarios incorrectos')
                        }
                        else {
                                const token = generateAccessToken(user.nombre, limpiar(req.body.remember) || false)
                                res.status(200).json(token)
                        }
                } else
                        res.status(401).send('Usuario no encontrado')
        } catch (err) {
                res.status(500).send(err)
                console.log("catch", err)
        }
}

exports.index = function (req, res) {
        res.sendFile(path.resolve('views/usuarios.html'))
}

exports.create = function (req, res) {
        try {
                var newUsuario = new Usuario(limpiar(req.body))
                newUsuario.save(function (err) {
                        if (err) {
                                res.status(400).send('Unable to save usuario to database' + err)
                        } else {
                                const token = generateAccessToken(limpiar(req.body.nombre), limpiar(req.body.remember) || false)
                                res.status(200).json(token)
                                // res.redirect('/usuarios/getusuario')
                        }
                })
        } catch (err) {
                res.status(500).send(err)
        }
}

exports.list = function (req, res) {
        Usuario.find({}).exec(function (err, usuarios) {
                if (err) {
                        return res.status(500).send(err)
                }
                res.render('getusuario', {
                        usuarios: usuarios
                })
        })
}

exports.getusuario = function (req, res) {
        Usuario.findById(limpiar(req.params.id), function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                res.json(usuario)
        })
}

// encuentra un usuario por su nombre en tiempo real
exports.live = function (req, res) {
        Usuario.find({ nombre: new RegExp(req.query.q) }, function (err, usuarios) {
                if (err) {
                        return res.status(500).send(err)
                }
                res.json(usuarios)
        })
}

// añade un usuario a la lista de amigos
exports.addfriend = function (req, res) {
        Usuario.findById(req.params.id, function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                if (usuario.amigos.indexOf(req.params.friend) === -1) {
                        usuario.amigos.push(req.params.friend)
                        usuario.save(function (err) {
                                if (err) {
                                        return res.status(500).send(err)
                                }
                                res.status(200).send('Amigo añadido: ' + req.params.friend)
                        })
                } else { return res.status(500).send('Amigo ya estaba añadido') }
        })
}

// elimina un usuario de la lista de amigos
exports.removefriend = function (req, res) {
        Usuario.findById(req.params.id, function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                var index = usuario.amigos.indexOf(req.params.friend)
                if (index > -1) {
                        usuario.amigos.splice(index, 1)
                        usuario.save(function (err) {
                                if (err) {
                                        return res.status(500).send(err)
                                }
                                res.status(200).send('Amigo eliminado')
                        })
                } else { return res.status(500).send('Amigo no encontrado') }
        })
}

exports.switchFriend = async function (req, res) {
        let user = await Usuario.getByName(limpiar(req.user.name)).catch(err => {
                console.log(err)
        })
        if (user) {
                let index = user.amigos.indexOf(req.body.friend_id)
                let friend = false
                // console.log(index)
                if (index === -1) {
                        // console.log("añadiendo amigo")
                        user.amigos.push(req.body.friend_id)
                        friend = true
                } else {
                        // console.log("eliminando amigo")
                        user.amigos.splice(index, 1)
                }
                const usuario = await Usuario.findByIdAndUpdate(user._id, { amigos: user.amigos }).catch(err => {
                        console.log(err)
                })
                if (usuario) {
                        res.status(200).send(friend)
                } else
                        return res.status(500).send(err)
        } else
                return res.status(500).send(err)
}




// lista todos los amigos de un usuario (devuelve objetos)
exports.listamigos = function (req, res) {
        Usuario.findById(req.params.id, function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                Usuario.find({ _id: { $in: usuario.amigos } }).exec(function (err, amigos) {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        res.json(amigos)
                })
        })
}

// busca un usuario por su nombre
exports.nombre = function (req, res) {
        Usuario.findOne({ nombre: limpiar(req.query.q) }, function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                res.json(usuario)
        })
}

// elimina un usuario por id
exports.delete = function (req, res) {
        // elimina el usuardio de la lista de amigos de otros usuarios
        Usuario.findByIdAndRemove(req.params.id, function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                } else {
                        Usuario.updateMany({}, { $pull: { amigos: req.params.id } }, function (err, usuario) {
                                if (err) {
                                        return res.status(500).send(err)
                                }
                                // res.status(200).send('Usuario eliminado de la lista de amigos de otros usuarios')
                                console.log('Usuario eliminado de la lista de amigos de otros usuarios')
                        })
                        res.status(200).send('Usuario eliminado')
                }
        })
}

// sólo para pruebas

exports.randomaddfriend = function (req, res) {
        Usuario.findById(req.params.id, function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                Usuario.aggregate([{ $sample: { size: 1 } }], function (err, amigo) {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        usuario.amigos.push(amigo[0]._id)
                        usuario.save(function (err) {
                                if (err) {
                                        return res.status(500).send(err)
                                }
                                res.status(200).send('Amigo añadido: ' + amigo[0].nombre)
                        })
                })
        })
}

exports.randomremovefriend = function (req, res) {
        Usuario.findById(req.params.id, function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                amigo = usuario.amigos[Math.random() * usuario.amigos.length | 0]
                var index = usuario.amigos.indexOf(amigo)
                if (index > -1) {
                        usuario.amigos.splice(index, 1)
                        usuario.save(function (err) {
                                if (err) {
                                        return res.status(500).send(err)
                                }
                                res.status(200).send('Amigo eliminado')
                        })
                } else { return res.status(500).send('Amigo no encontrado :(') }
        })

}

exports.getFriends = async function (req, res) {
        const nombre = limpiar(req.user.name)
        Usuario.getByName(nombre, function (nombre) {
        }).populate('amigos').exec(function (err, usuario) {
                if (err) {
                        return res.status(500).send(err)
                }
                res.status(200).send(usuario.amigos)
        })
}


// match users by interests
exports.match = function (req, res) {
        try {
                const nombre = limpiar(req.query.name)
                const filter = new RegExp(nombre)
                Usuario.getByName(limpiar(req.user.name)).then(function (usuario) {
                        Usuario.find({
                                nombre: filter,
                                $and: [
                                        { intereses: { $in: usuario?.imparte } },
                                        { imparte: { $in: usuario?.intereses } }]
                        })
                                .populate('imparte')
                                .populate('intereses')
                                .sort({ [req.query.sort || 'calificacion']: [req.query.asc || "desc"] })
                                .limit(req.query.limit || 50)
                                .then(function (usuarios) {
                                        for (let amigo in usuarios) {
                                                usuarios[amigo].friend = usuario.amigos.indexOf(usuarios[amigo]._id) > -1
                                        }
                                        res.status(200).json(usuarios)
                                })
                })
        } catch (err) {
                console.log(err)
        }
}

exports.meetin = function (req, res) {
        res.render('meetin')
}

exports.test = function (req, res) {
        let password = req.params.password
        let salt = bcryptjs.genSaltSync(10) // 10 es la cantidad de iteraciones
        password = bcryptjs.hashSync(password, salt) // encriptamos el password
        return res.status(200).send(password)
}