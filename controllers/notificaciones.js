const path = require('path')
const Notificacion = require('../models/notificacion')
const Usuario = require('../models/usuario')
const limpiar = require('../middlewares/limpiar')

exports.amigos = async (req, res) => {
        try {
                const user = await Usuario.getIDByName(limpiar(req.user.name))
                const data = await Notificacion.find({
                        $or: [
                                { user: { $in: this.receptores } },
                                { user: this.id_creador }]
                })
                        .sort({ fecha_creado: limpiar(req.body.sort) || 'desc' })
                // const data = await Notificacion.find({
                //         $match:
                //         {
                //                 $group: {
                //                         if(amigo) {
                //                                 $and: [
                //                                         { amigo: { $in: receptores } },
                //                                         { amigo: id_creador }], {
                //                                         $or: [
                //                                                 { user: { $in: receptores } },
                //                                                 { user: id_creador }]
                //                                 }
                //                         }
                //                 }
                //         }
                // })
                //         .sort({ fecha_creado: limpiar(req.body.sort) || 'desc' })

                if (data) {
                        for (let d in data) {
                                // calcula los minutos, horas y días que han pasado desde la fecha de creación de la notificación
                                data[d].enviado = calcularTiempo(data[d].fecha_creado)
                                data[d].propio = data[d].id_creador.equals(user._id)
                        }

                        res.status(200).send(data)
                }
        } catch (error) {
                console.log(error)
                res.status(500).send(error)
        }
}

exports.list = async (req, res) => {
        try {
                const amigo_id = limpiar(req.body?.user_id)
                const user = await Usuario.getIDByName(limpiar(req.user.name))
                const data = await Notificacion.find({
                        $and: [{
                                $or: [
                                        { id_creador: amigo_id },
                                        { amigo_id: { $in: this.receptores } }
                                ]
                        }, {
                                $or: [
                                        { user: { $in: this.receptores } },
                                        { user: this.id_creador }]
                        }
                        ]
                })
                        .sort({ fecha_creado: limpiar(req.body.sort) || 'desc' })
                        .populate('id_creador', 'nombre avatar')
                        .populate('receptores', 'nombre avatar')

                if (data) {
                        for (let d in data) {
                                // calcula los minutos, horas y días que han pasado desde la fecha de creación de la notificación
                                data[d].enviado = calcularTiempo(data[d].fecha_creado)
                                data[d].propio = data[d].id_creador.equals(user._id)
                        }

                        res.status(200).send(data)
                }
        } catch (error) {
                console.log(error)
                res.status(500).send(error)
        }
}

exports.create = async (req, res) => {
        try {
                req.body.id_creador = await Usuario.getIDByName(limpiar(req.user.name))
                console.log(req.body.receptores)
                const data = await Notificacion.create((req.body))
                res.json(data)
        } catch (error) {
                console.log(error)
                res.status(500).send(error)
        }
}

exports.update = (req, res) => {
        try {
                Notificacion.findOneAndUpdate(limpiar(req.params.id), limpiar(req.body)).then(function (err, notificacion) {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        res.status(200).json(notificacion)
                })
        } catch (error) {
                res.status(500).send(error)
        }
}

exports.delete = (req, res) => {
        // console.log(limpiar(req.params.id))
        try {
                Notificacion.findByIdAndUpdate(limpiar(req.params.id), { borrado: true }, (err, notificacion) => {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        res.status(200).send('Notificacion eliminada')
                })
        } catch (error) {
                res.status(500).send(error)
        }
}


exports.findById = async (req, res) => {
        try {
                const data = await Notificacion.getById(limpiar(req.params.id))
                res.json(data)
        } catch (error) {
                res.status(500).send(error)
        }
}

function calcularTiempo(fecha_creado) {
        let d = Math.abs(Date.now() - new Date(fecha_creado)) / 1000
        let r = {}
        let s = {
                año: 31536000,
                mes: 2628000,
                sem: 604800,
                d: 86400,
                hr: 3600,
                min: 60,
        }

        for (let i in s) {
                r[i] = Math.floor(d / s[i])
                d -= r[i] * s[i]
        }

        for (let i in s) {
                if (r[i] > 0) {
                        return `${r[i]} ${i}${r[i] > 1 && i == 'año' ? 's' : ''}`
                }
        }
        return 'ahora'
}