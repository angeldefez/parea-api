const path = require('path')
const Conocimiento = require('../models/conocimiento')
const limpiar = require('../middlewares/limpiar')

exports.list = async (req, res) => {
        try {
                const data = await Conocimiento.getAll()
                res.json(data)
        } catch (error) {
                res.status(500).send(error)
        }
}

exports.create = async (req, res) => {
        try {
                const data = await Conocimiento.create(limpiar(req.body))
                res.json(data)
        } catch (error) {
                res.status(500).send(error)
        }
}

exports.update = (req, res) => {
        try {
                Conocimiento.findAndUpdate(limpiar(req.params.id), limpiar(req.body)).then(function (err, conocimiento) {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        res.status(200).json(conocimiento)
                })
        } catch (error) {
                res.status(500).send(error)
        }
}

exports.delete = (req, res) => {
        try {
                Conocimiento.findByIdAndRemove(limpiar(req.params.id), (err, conocimiento) => {
                        if (err) {
                                return res.status(500).send(err)
                        }
                        res.status(200).send('Conocimiento eliminado')
                })
        } catch (error) {
                res.status(500).send(error)
        }
}


exports.findById = async (req, res) => {
        // Conocimiento.getById(req.params.id).then(function (conocimiento) {
        //         res.json(conocimiento)
        // })
        try {
                const data = await Conocimiento.getById(limpiar(req.params.id))
                res.json(data)
        } catch (error) {
                res.status(500).send(error)
        }
}

exports.findByName = async (req, res) => {
        try {
                const data = await Conocimiento.getByName(limpiar(req.params.name))
                res.json(data)
        } catch (error) {
                res.status(500).send(error)
        }
}
