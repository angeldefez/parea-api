const express = require('express')
const app = express()
const jwt = require('jsonwebtoken')

function authToken(req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (!token) return res.status(401).json({ error: 'No token provided' })

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    if (err)
      return res.status(403).json({ error: 'Failed to authenticate token' })
    req.user = user

    next()
  })
}

function generateAccessToken(user, rememberMe = false) {
  const exp_time = (rememberMe === true) ? "7d" : "1d"
  const token = jwt.sign({ name: user }, process.env.TOKEN_SECRET, { expiresIn: exp_time })
  let env = app.get('env')

  return token
}

// app.post('/api/createNewUser', (req, res) => {
// // ...

//     const token = generateAccessToken({ username: req.body.username });
//     res.json(token);

// // ...
// })


// get token from fetch request
// const token = await res.json();

// set token in cookie
// document.cookie = `token=${token}`

// console.log(require('crypto').randomBytes(64).toString('hex'))

module.exports = { authToken, generateAccessToken }
