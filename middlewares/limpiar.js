// limpia el payload de los datos que no sean necesarios
// si encuentra un objeto con una key que empieza con $, lo elimina

function limpiar(query) {
    if (query instanceof Object) {
        for (var key in query) {
            if (/^\$/.test(key))
                delete query[key]
            else
                limpiar(query[key]) // repetimos por si hay más ocurrencias
        }
    }

    return query
}

module.exports = limpiar