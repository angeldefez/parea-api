// configuramos el servidor con los parametros de configuración y los middlewares que vamos a usar

const express = require('express'),
  app = express(),
  db = require('./db'),
  api = require('./routes'),
  cors = require('cors'),
  morgan = require('morgan'),
  path = __dirname + '/views/',
  port = process.env.PORT || 8080, // definimos el puerto de escucha
  node_env = process.env.node_env || 'development'
// jwt = require('jsonwebtoken'),
// mi_token = process.env.TOKEN_SECRET || 'supersecret'

app.set('port', port)
app.set('env', node_env)
app.disabled('x-powered-by')
app.use(cors())
app.use(morgan('dev'))

app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path))

// rutas
app.use('/api', api)

// iniciamos el servidor

app.listen(port, () => {
  console.log(`[+] App escuchando en el puerto ${port}!`)
})