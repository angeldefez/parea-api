const express = require('express')
const router = express.Router()
const quedadas = require('../controllers/quedadas')

router.route('/')
    .get(quedadas.list)
    .post(quedadas.create)

router.route('/:id')
    .get(quedadas.findById)
    .put(quedadas.update)
    .delete(quedadas.delete)

module.exports = router