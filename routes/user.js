const express = require('express')
const router = express.Router()
const usuario = require('../controllers/user')

// router.get('/', (req, res) =>{
//     usuario.index(req,res)
// });

router.route('/')
    .get(usuario.get)

router.route('/update')
    .put(usuario.update)

router.route('/login')
    .put(usuario.login)

router.route('/register')
    .post(usuario.create)

router.route('/switchFriend')
    .put(usuario.switchFriend)

router.route('/friends')
    .get(usuario.getFriends)

router.get('/match', (req, res) => {
    usuario.match(req, res)
})

// router.post('/', (req, res) => {
//     usuario.create(req, res)
// })

// router.get('/:id', (req, res) => {
//     usuario.list(req,res)
// })

router.get('/:id/friends', (req, res) => {
    usuario.listfriends(req, res)
})

router.put('/:id/friends/:idf', (req, res) => {
    usuario.addfriend(req, res)
})

router.delete('/:id/friends/:idf', (req, res) => {
    usuario.removefriend(req, res)
})

router.post('/:id/update', (req, res) => {
    usuario.update(req, res)
})

router.delete('/:id/delete', (req, res) => {
    usuario.delete(req, res)
})

module.exports = router

// mover a users.js
// router.get('/users', (req, res) => {
//     usuario.list(req,res)
// })

// router.get('/users/:nombre', (req, res) => {
//     usuario.nombre(req,res)
// })

// router.get('/users/:id', (req, res) => {
//     usuario.usuario(req,res)
// })

// router.get('/users/live', (req, res) => {
//     usuario.live(req,res)
// })

// sólo para pruebas
router.get('/aleatorio/:id/friends', (req, res) => {
    usuario.randomaddfriend(req, res)
})

router.delete('/aleatorio/:id/friends', (req, res) => {
    usuario.randomremovefriend(req, res)
})

router.get('/meetin', (req, res) => {
    usuario.meetin(req, res)
})

router.get('/test/:password', (req, res) => {
    usuario.test(req, res)
})