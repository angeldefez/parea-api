const express = require('express')
const router = express.Router()
const conocimientos = require('../controllers/conocimientos')

// router.get('/', (req, res) => {
//     conocimientos.list(req,res)
// })

router.route('/')
    .get(conocimientos.list)
    .post(conocimientos.create)

router.route('/:id')
    .get(conocimientos.findById)
    .put(conocimientos.update)
    .delete(conocimientos.delete)

router.route('/:name')
    .get(conocimientos.findByName)



module.exports = router