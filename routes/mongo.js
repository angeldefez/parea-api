// ruta para poblar la base de datos en modo development

const express = require('express')
const router = express.Router()
const db = require('../db')

router.route('/')
    .put((req, res) => {
        const env = process.env.NODE_ENV || 'development'
        if (env === 'development') {
            const bbdd = require('../scripts/importar')
            if (bbdd)
                res.status(200).send('Base de datos importada')
            else
                res.status(500).send('Error al importar la base de datos')
        } else {
            res.status(500).send('No puedes poblar la base de datos en producción')
        }
    })
    .get((req, res) => {
        // return 500 if database is empty
        db.connection.db.listCollections({ name: 'conocimientos' })
            .next(function (err, collinfo) {
                if (collinfo) {
                    // The collection exists
                    res.status(200).send('La base de datos no está vacía')
                }
            });
    })

module.exports = router