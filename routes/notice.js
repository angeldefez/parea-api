const express = require('express')
const router = express.Router()
const notificacion = require('../controllers/notificaciones')

router.route('/')
    .get(notificacion.list)
    .post(notificacion.create)

router.route('/:id')
    .get(notificacion.findById)
    .put(notificacion.update)
    .delete(notificacion.delete)

module.exports = router